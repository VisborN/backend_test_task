package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"testing"

	"gitlab.com/VisborN/backend_test_task/pb"
)

func TestCreateArticle(t *testing.T) {
	article := pb.Article{Header: "best Article", Id: "mtestArticle"}
	articleJSON, err := json.Marshal(article)
	if err != nil {
		t.Errorf("failed marshal article %+v", err)
	}
	resp, err := http.Post("http://127.0.0.1:8080/createArticle", "application/json", bytes.NewReader(articleJSON))
	if err != nil {
		t.Errorf("failed get article %+v", err)
	}
	if resp.StatusCode != 200 {
		t.Errorf("failed get article status!=200 %+v", resp)
	}
}

func TestGetArticle(t *testing.T) {
	resp, err := http.Get("http://127.0.0.1:8080/articles/mtestArticle")
	if err != nil {
		t.Errorf("failed get article %+v", err)
	}
	if resp.StatusCode != 200 {
		t.Errorf("failed get article status!=200 %+v", resp)
	}
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("failed read body %+v", err)
	}
	bodyString := string(bodyBytes)
	t.Logf("got body %s", bodyString)
}
