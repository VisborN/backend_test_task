package cmd

import (
	"log"

	"gitlab.com/VisborN/backend_test_task/pb"

	// Import GORM-related packages.
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	// Necessary in order to check for transaction retry error codes.
)

var addrDBConnect string

func connectDB() *gorm.DB {
	// Connect to the "bank" database as the "maxroach" user.
	db, err := gorm.Open("postgres", addrDBConnect)
	if err != nil {
		log.Fatalf("error connecting db %+v", err)
	}

	db.AutoMigrate(&pb.Article{})
	return db
}
