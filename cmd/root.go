package cmd

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/nats-io/nats.go"
	stan "github.com/nats-io/stan.go"
	"github.com/spf13/cobra"
)

var stanPort int
var stanURL string
var natsUserCreds string
var stanClientID string
var stanClusterID string

var rootCmd = &cobra.Command{
	Use:   "backend_test_task",
	Short: "A brief description of your application",
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {

	rootCmd.PersistentFlags().StringVarP(&stanURL, "stanurl", "u", nats.DefaultURL, "nats url used")
	rootCmd.PersistentFlags().StringVarP(&natsUserCreds, "natsUserCreds", "L", "", "nats user credentials file")
	rootCmd.PersistentFlags().StringVarP(&stanClientID, "stanclient", "N", strconv.FormatInt(int64(os.Getpid()), 10), "client id used by NATS streaming")
	rootCmd.PersistentFlags().StringVarP(&stanClusterID, "stancluster", "C", "test-cluster", "cluster id used by NATS streaming")
}

func stanConnect() stan.Conn {

	opts := []nats.Option{}
	if natsUserCreds != "" {
		opts = append(opts, nats.UserCredentials(natsUserCreds))
	}

	nc, err := nats.Connect(stanURL, opts...)
	if err != nil {
		log.Fatalf("can't connect to nats %#v\n", err)
	}
	sc, err := stan.Connect(stanClusterID, stanClientID, stan.NatsConn(nc))
	if err != nil {
		log.Fatalf("can't connect to nats %#v\n", err)
	}
	fmt.Printf("connected to stan as cluster id:%s client id:%s", stanClusterID, stanClientID)
	return sc

}
