package cmd

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	proto "github.com/gogo/protobuf/proto"
	"github.com/nats-io/nats.go"
	stan "github.com/nats-io/stan.go"
	"github.com/spf13/cobra"
	"gitlab.com/VisborN/backend_test_task/pb"

	"github.com/jinzhu/gorm"
)

const (
	createArticleChan = "createArticle"
	getArticleChan    = "getArticle"
)

// storageCmd represents the storage command
var storageCmd = &cobra.Command{
	Use:   "storage",
	Short: "A brief description of your command",
	Run:   storageMain,
}

func init() {
	rootCmd.AddCommand(storageCmd)

	storageCmd.PersistentFlags().StringVarP(&addrDBConnect, "addrDB", "a",
		"postgresql://maxroach@localhost:26257/newspaper?sslmode=disable", "address used to connect to db in gorm")
}

func storageMain(cmd *cobra.Command, args []string) {
	fmt.Println("storage called")

	db := connectDB()
	defer db.Close()

	sc := stanConnect()
	nc := sc.NatsConn()
	defer sc.Close()

	subGetArticle, err := nc.Subscribe(getArticleChan, getArticleSubscriber(nc, db))
	if err != nil {
		fmt.Printf("can't subscribe to getArticle %+v\n", err)
		return
	}
	_, err = sc.QueueSubscribe(
		createArticleChan,
		"main-create-queue",
		createArticleSubscriber(sc, db),
		stan.SetManualAckMode(),
		stan.AckWait(time.Minute),
		stan.DurableName("dur"))
	if err != nil {
		fmt.Printf("can't subscribe to createArticle %#v\n", err)
		return
	}
	sigc := make(chan os.Signal, 1)

	signal.Notify(
		sigc,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
	)

	<-sigc
	fmt.Printf("unsubscribe\n")

	subGetArticle.Unsubscribe()

}

func getArticleSubscriber(nc *nats.Conn, db *gorm.DB) func(m *nats.Msg) {
	return func(m *nats.Msg) {
		var message pb.GetArticle
		var response pb.ResponseGetArticle
		err := proto.Unmarshal(m.Data, &message)
		if err != nil {
			response = pb.ResponseGetArticle{Error: 1}
		} else {
			response.Article = &pb.Article{}
			err := db.
				Where("id = ?", message.Id).
				First(&response.Article).Error
			if err != nil {
				if gorm.IsRecordNotFoundError(err) {
					response = pb.ResponseGetArticle{Exists: false}
				} else {
					log.Printf("error occured when getting article from db %+v", err)
					response = pb.ResponseGetArticle{Error: 4}
				}
			} else {
				response.Exists = true
			}
		}
		responseProto, err := proto.Marshal(&response)
		if err != nil {
			response = pb.ResponseGetArticle{Error: 2}
			responseProto, _ = proto.Marshal(&response)
		}
		err = m.Respond(responseProto)
		if err != nil {
			fmt.Printf("error responding on message getArticle %+v\n", err)
		}
		fmt.Printf("Received a message: %+v\n", message)
	}
}

func createArticleSubscriber(sc stan.Conn, db *gorm.DB) func(m *stan.Msg) {
	return func(m *stan.Msg) {
		var message pb.CreateArticle
		var response pb.ResponseCreateArticle

		err := proto.Unmarshal(m.Data, &message)
		if err != nil {
			fmt.Printf("can't unmarshal a message: %#v\n", m)
			m.Ack() //acknowledge so this message wouldnt try to process anymore
			response.Error = 2
		} else {
			err = db.Create(message.Article).Error
			if err != nil {
				fmt.Printf("Couldnt create in db: %+v %+v\n", message, err)
				response.Error = 1

				m.Ack()
			} else {
				m.Ack()
			}
		}

		nc := sc.NatsConn()
		responseProto, err := proto.Marshal(&response)
		if err != nil {
			response = pb.ResponseCreateArticle{Error: 3}
			responseProto, _ = proto.Marshal(&response)
		}
		nc.Publish(message.ResponseUUID, responseProto)
	}
}
