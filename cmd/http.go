package cmd

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	proto "github.com/gogo/protobuf/proto"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/nats-io/nats.go"
	stan "github.com/nats-io/stan.go"
	"github.com/spf13/cobra"
	"gitlab.com/VisborN/backend_test_task/pb"
)

var httpPort int
var enableGet bool
var enablePost bool

// httpCmd represents the http command
var httpCmd = &cobra.Command{
	Use:   "http",
	Short: "Starts http server",
	Run:   httpMain,
}

func init() {
	rootCmd.AddCommand(httpCmd)

	httpCmd.PersistentFlags().IntVarP(&httpPort, "httpport", "p", 50001, "port used by this http client")
	httpCmd.PersistentFlags().BoolVarP(&enableGet, "get", "G", false, "enables get http requests")
	httpCmd.PersistentFlags().BoolVarP(&enablePost, "post", "P", false, "enables post http requests")
}

func httpMain(cmd *cobra.Command, args []string) {
	fmt.Println("http called")
	sc := stanConnect()
	defer sc.Close()

	r := mux.NewRouter()
	if enableGet {
		r.HandleFunc("/articles/{id:[a-zA-Z0-9]+}", getArticleHandler(sc.NatsConn())).
			Methods("GET")
	}
	if enablePost {
		r.HandleFunc("/createArticle", createArticleHandler(sc)).
			Methods("POST").
			Headers("Content-Type", "application/json")
	}

	srv := &http.Server{
		Handler:      r,
		Addr:         "127.0.0.1:" + strconv.FormatInt(int64(httpPort), 10),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	err := srv.ListenAndServe()
	if err != nil {
		fmt.Printf("error ListenAndServe %+v", err)
	}

}

func getArticleHandler(nc *nats.Conn) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		vars := mux.Vars(r)
		message := pb.GetArticle{
			Id: vars["id"],
		}
		messageProto, err := proto.Marshal(&message)
		if err != nil {

		}
		responseNats, err := nc.Request(getArticleChan, messageProto, time.Second*5)

		var response pb.ResponseGetArticle
		if err != nil {
			response = pb.ResponseGetArticle{Error: 11}
		}
		err = proto.Unmarshal(responseNats.Data, &response)
		if err != nil {
			response = pb.ResponseGetArticle{Error: 11}
		}

		if response.Error != 0 {
			w.WriteHeader(500)
			fmt.Fprintf(w, "500 Internal Server Error %d", response.Error)
		} else if !response.Exists || response.Article == nil {
			w.WriteHeader(404)
			fmt.Fprintf(w, "404 Article not found")
		} else {
			w.Header().Add("Content-Type", "application/json")
			err := json.NewEncoder(w).Encode(response.Article)
			if err != nil {
				w.WriteHeader(500)
				fmt.Fprintf(w, "500 Internal Server Error json encode")
			}
		}

	}
}

func createArticleHandler(sc stan.Conn) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		message := pb.CreateArticle{
			Article:      &pb.Article{},
			ResponseUUID: uuid.New().String(),
		}
		err := json.NewDecoder(r.Body).Decode(message.Article)
		if err != nil {

		}
		message.Article.Date = time.Now().Format(time.RFC3339)
		messageProto, err := proto.Marshal(&message)
		if err != nil {

		}
		nc := sc.NatsConn()
		sub, err := nc.SubscribeSync(message.ResponseUUID)
		if err != nil {
			w.WriteHeader(500)
			fmt.Fprintf(w, "500 Internal Server Error couldnt publish article")
			return
		}
		err = sc.Publish(createArticleChan, messageProto)
		if err != nil {
			w.WriteHeader(500)
			fmt.Fprintf(w, "500 Internal Server Error couldnt publish article")
			return
		}

		msg, err := sub.NextMsg(time.Second)
		if err != nil {
			w.WriteHeader(500)
			fmt.Fprintf(w, "500 Internal Server Error couldnt get response from storage")
			return
		}
		var response pb.ResponseCreateArticle
		err = proto.Unmarshal(msg.Data, &response)
		if err != nil || response.Error != 0 {
			w.WriteHeader(500)
			fmt.Fprintf(w, "500 Internal Server Error couldnt get response from storage %d %+v", response.Error, err)
			return
		}
		if response.AlreadyExists {
			w.WriteHeader(409)
			fmt.Fprintf(w, "article already exists")

		}

		fmt.Fprintf(w, "success")
	}
}
