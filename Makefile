

PROTOC_ZIP=protoc-3.7.1-linux-x86_64.zip
/usr/local/bin/protoc:
	go get github.com/gogo/protobuf/protoc-gen-gogofast
	curl -OL https://github.com/protocolbuffers/protobuf/releases/download/v3.7.1/$(PROTOC_ZIP)
	sudo unzip -o $(PROTOC_ZIP) -d /usr/local bin/protoc
	sudo unzip -o $(PROTOC_ZIP) -d /usr/local 'include/*'
	rm -f $(PROTOC_ZIP)

install: #/usr/local/bin/protoc 
	go get github.com/nats-io/nats-streaming-server
	
	docker network create -d bridge roachnet
	mkdir -p cockroach-data/roach1
	docker run -d \
		--name=roach1 \
		--hostname=roach1 \
		--net=roachnet \
		-p 26257:26257 -p 8081:8080  \
		-v "${PWD}/cockroach-data/roach1:/cockroach/cockroach-data"  \
		cockroachdb/cockroach start \
		--insecure \
		--join=roach1
	docker exec -it roach1 ./cockroach init --insecure
	docker exec -it roach1 ./cockroach sql --insecure \
		-e "CREATE USER IF NOT EXISTS maxroach; CREATE DATABASE IF NOT EXISTS newspaper;GRANT ALL ON DATABASE newspaper TO maxroach;"
	docker stop roach1
services:
	docker start roach1
	nats-streaming-server
	
http:
	go run main.go http --get --post -p 8080

storage:
	go run main.go storage

test:
	go test -v
	docker exec -it roach1 ./cockroach sql --insecure \
		-e "SELECT * from newspaper.articles;"
proto:
	protoc --gogofast_out=. ./pb/messages.proto
rm:
	docker stop roach1
	docker rm roach1
	docker network rm roachnet
	sudo rm -rf cockroach-data
