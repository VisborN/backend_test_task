```
make install 
make services
make storage
make http

go test -v

curl 127.0.0.1:8080/articles/mtestArticle
curl --request POST -H "Content-Type: application/json" 127.0.0.1:8080/createArticle -d '{"id":"mtestArticle","header":"best Article"}'
```